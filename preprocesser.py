from rasa.nlu.components import Component
from rasa.nlu import utils
from rasa.nlu.model import Metadata
from unidecode import unidecode
from pyvi import ViUtils
from typing import Any, Dict, List, Text
from rasa.shared.nlu.training_data.message import Message
from rasa.nlu.constants import TOKENS_NAMES
from rasa.core.agent import Agent
import os
import pickle
import re
import nltk
import requests
from nltk.tokenize.treebank import TreebankWordDetokenizer
from rasa.utils.endpoints import EndpointConfig 

class VietnamesePreprocesser(Component):

    name = "VietnamesePreprocesser"
    provides = ["entities"]
    requires = ["message"]

    def __init__(self, component_config=None):
        print("Preloading accent model" )
        #nltk.download('punkt')
        # tai cai model tai day : https://github.com/vapormusic/mfe_models/releases/
        #model_dir = "./fasttext/"
        #with open(os.path.join(model_dir, 'kneserney_1st_ngram_model.pkl'), 'rb') as fin:
        #     self.model_loaded = pickle.load(fin)
         
        super().__init__(component_config)

    def train(self, training_data, cfg, **kwargs):
        """Not needed, because the the model is pretrained"""
        pass
        

          
    def process(self, message : Message, **kwargs):

            
        def isEnglish(s):
            x = re.search("[ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéTêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]", s)
            if x:
              return False
            else:
              return True  
        try:
         text_data = message.get("text")
         #print("lmao:" + message.get("text"))
         if(isEnglish(text_data)):
          new_message = unidecode(text_data).lower()

         else:
          new_message = unidecode(text_data).lower()

        
         #print("lmao2:" + new_message)
         message.set("text", new_message)
        except:
         pass
          

    def persist(self, file_name, model_dir):
        """Pass because a pre-trained model is already persisted"""
        pass
        
    
	
             
