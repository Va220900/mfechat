import asyncio
import json
import re
from flask import Flask, request
from flask_cors import CORS, cross_origin
from rasa.core.agent import Agent
from rasa.utils.endpoints import EndpointConfig
from werkzeug.exceptions import abort
import sys
import os

app = Flask(__name__)
CORS(app)
cors = CORS(app, resources={r"/*": {"origins": "*"}})
app.config['CORS_HEADERS'] = 'Content-Type'
action_endpoint = "https://fathomless-oasis-50487.herokuapp.com/webhook"


def agent_get(orgid):
    orgId = {"co_dau": "/app/models/co_dau.tar.gz",
             "ko_dau": "/app/models/khong_dau.tar.gz"}
    print(orgid)
    print(orgId[orgid])
    agent = Agent.load(orgId[orgid], action_endpoint =EndpointConfig(action_endpoint))
    return agent


async def process(agent, msg, senderid):
    output = await agent.handle_text(msg, sender_id = senderid)
    print(output)
    return output

async def process2(agent, msg, senderid):
    output = await agent.handle_text(msg, sender_id = senderid)
    print(output)
    response = {
        'recipient': {'id': senderid},
        'message': {}
    }
    response['message']['text'] = output[0]['text']
    r = requests.post(
        'https://graph.facebook.com/v2.6/me/messages?access_token=' + access_token, json=response)
    return output

from flask import Flask, request, Response
import requests, json, random, os


# env_variables
# token to verify that this bot is legit
verify_token = "mfechat"
# token to send messages through facebook messenger
access_token = "EAAMkcdpZBH38BAHWoCeyg7ZC1tyMnZCdm47ZA3GmBUnEfHs3g0XcEqThUtkz2CT8c7ZBicQrhgSXiop4naiCcBPmbPkciyDoZAgyg5oDS8WCn2HZCcJrqvBHaVLNzVd5ZCZCwUBMNvImNV5QwOMZBKGBAp5Vzg6Lla2ypjBE5G0cZAxh8YhFA4UKNVa"

@app.route('/webhook', methods=['GET'])
def webhook_verify():
    if request.args.get('hub.verify_token') == verify_token:
        return request.args.get('hub.challenge')
    return "Wrong verify token"

@app.route('/webhook', methods=['POST'])
def webhook_action():
    data = json.loads(request.data.decode('utf-8'))
    for entry in data['entry']:
        try:
            user_message = entry['messaging'][0]['message']['text']
            user_id = entry['messaging'][0]['sender']['id']
            response = {
                'recipient': {'id': user_id},
                'message': {}
            }
            vietregex ="[ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]";
            agent = "ko_dau"

            if (re.search(vietregex,user_message) != None):
                agent = "co_dau"
            asyncio.run(process2(agent_get(agent), user_message, user_id))
        except:
            pass

    return Response(response="EVENT RECEIVED",status=200)

@app.route('/webhook_dev', methods=['POST'])
def webhook_dev():
    # custom route for local development
    data = json.loads(request.data.decode('utf-8'))
    user_message = data['entry'][0]['messaging'][0]['message']['text']
    user_id = data['entry'][0]['messaging'][0]['sender']['id']
    response = {
        'recipient': {'id': user_id},
        'message': {'text': handle_message(user_id, user_message)}
    }
    return Response(
        response=json.dumps(response),
        status=200,
        mimetype='application/json'
    )

def handle_message(user_id, user_message):
    # DO SOMETHING with the user_message ... ¯\_(ツ)_/¯
    return "Hello "+user_id+" ! You just sent me : " + user_message

@app.route('/privacy', methods=['GET'])
def privacy():
    # needed route if you need to make your bot public
    return "This facebook messenger bot's only purpose is to query answers. That's all. We don't use it in any other way."

@app.route('/', methods=['GET'])
def index():
    return "Hello there, I'm a facebook messenger bot."



@app.route("/message", methods=["POST"])
@cross_origin(origin="*")
def new_message():
    if not request.json:
        abort(400)
    orgId  = request.json["agent"]
    current_agent = agent_get(orgId)
    user = request.json["sender"]
    message = request.json["message"]
    #current_agent.handle_text(text_message=message, sender_id = user)
    print(user)

    message = asyncio.run(process(current_agent,message,user))
    message = json.dumps(message)
    return message


if __name__ == "__main__":
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port, debug=True, use_reloader=False)
    
