# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"

from typing import Any, Text, Dict, List
from rasa_sdk import Action, Tracker
from rasa_sdk.executor import CollectingDispatcher
from unidecode import unidecode


class MajorSpecifier(Action):

    def name(self) -> Text:
        return 'action_major_specifier'

    def run(self,dispatcher: CollectingDispatcher,tracker: Tracker,domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        actuary = ["act","actuary","dinh phi bao hiem","quan tri rui ro", "dpbh","qtrr","actuarial"]
        toankt = ["toan kt","toan kinh te","tkt"]
        dseb = ["data science","khoa hoc du lieu","ds","khdl","cntt","du lieu", "machine learning","ai","ml"]
        user_input = unidecode(tracker.latest_message.get('text')).lower()
        intent = tracker.latest_message['intent'].get('name')
        if any(map(user_input.__contains__, actuary)):
           intent = intent + "_actuary"
        elif any(map(user_input.__contains__, toankt)):
           intent = intent + "_tkt"
        elif any(map(user_input.__contains__, dseb)):
           intent = intent + "_datascience"
        else :
           intent = intent + "_actuary"   
        print(tracker.latest_message)   
        print(intent)
        dispatcher.utter_message(template="utter_"+intent)
        return []
        
class MajorSpecifier2(Action):

    def name(self) -> Text:
        return 'action_major_specifier2'

    def run(self,dispatcher: CollectingDispatcher,tracker: Tracker,domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        actuary = ["act","actuary","dinh phi bao hiem","quan tri rui ro", "dpbh","qtrr","actuarial"]
        toankt = ["toan kt","toan kinh te","tkt"]
        dseb = ["data science","khoa hoc du lieu","ds","khdl","cntt","du lieu", "machine learning","ai","ml"]
        user_input = unidecode(tracker.latest_message.get('text')).lower()
        intent = tracker.latest_message['response_selector']['default']['response'].get('template_name')
        if any(map(user_input.__contains__, actuary)):
           intent = intent + "_actuary"
        elif any(map(user_input.__contains__, toankt)):
           intent = intent + "_tkt"
        elif any(map(user_input.__contains__, dseb)):
           intent = intent + "_datascience"
        else :
           intent = intent + ""   
        print(tracker.latest_message)   
        print(intent)
        dispatcher.utter_message(template="utter_"+intent)
        return []        
