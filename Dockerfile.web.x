FROM ubuntu:18.04

ENTRYPOINT []

RUN apt-get update && apt-get install -y python3 python3-pip && python3 -m pip install --no-cache --upgrade pip 
RUN pip install https://github.com/trungtv/vi_spacy/raw/master/packages/vi_spacy_model-0.2.1/dist/vi_spacy_model-0.2.1.tar.gz
RUN pip3 install --no-cache rasa[full]
RUN pip3 install rasa-x --extra-index-url https://pypi.rasa.com/simple --use-deprecated=legacy-resolver

ADD . /app/
#Copy actions folder to working directory
COPY ./ /app/
COPY requirements.txt ./
RUN pip install -r requirements.txt
RUN chmod +x /app/start-rasa-x.sh
RUN cd app/
RUN mkdir fasttext
RUN cd fasttext/
RUN apt-get install -y wget
RUN wget https://github.com/vapormusic/mfe_models/releases/download/v1.0/kneserney_1st_ngram_model.pkl
RUN cd 
CMD /app/start-rasa-x.sh


#Extend the official Rasa SDK image
#FROM rasa/rasa:2.3.1-full

#Use subdirectory as working directory
#WORKDIR /app

#Copy any additional custom requirements, if necessary (uncomment next line)
#COPY requirements.txt ./

#Change back to root user to install dependencies
#USER root

#Copy actions folder to working directory
#COPY ./ /app/

#Install extra requirements for actions code, if necessary (uncomment next line)
#RUN pip install -r requirements.txt

#RUN /opt/venv/bin/python -m pip install --upgrade pip
#By best practices, don't run the code with root user
#RUN chmod +x /app/start-service.sh
#USER 1001

#CMD $(echo “rasa run -p $PORT -m models --credentials credentials.yml --enable-api --log-file out.log --endpoints endpoints.yml” | sed ‘s/=//’)
